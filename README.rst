====================================================
Web testing with Robot Framework and SeleniumLibrary
====================================================

This project (Data driven test cases on web testing) is using `Robot Framework` 
as the test automation framework and SeleniumLibrary as the test library
Test logs and reports are generated at the end of test execution

Python Virtual Environment
==========================

Installation of the packages are made in a virtual Environment
The Browser Drivers are all provided in the Script folder of Python
Interpreter (python.exe)) location of the virtual environment

Python
Robot Framework
SeleniumLibrary
All the required packages

Demo application
================

The demo application is provided by robotframework.org. See the Reference section
It is a login page as shown below.

The login details, use ``demo`` as user name and ``mode`` as password
You will be directed to the welcome page after a successful login, 
otherwise you will see the error page for unsuccessful attempt.

For more details, see the Reference link

.. figure:: demoapp.png

Test cases
==========

Test case files and a resource file are located in the ``login_tests`` directory.

`valid_login.robot`_
    This test suite has has only one test for valid login.

    Test workflow uses keywords defined in the imported resource file.

`invalid_login.robot`_
    A test suite containing tests for invalid login.

    The test uses single keyword, specified with the ``Test Template`` setting. They are called
    with different arguments to cover different scenarios.

    This suite also uses also the pre and post conditions for setups and teardowns in
    different levels.

`gherkin_login.robot`_
    A test suite with a single Gherkin style test.

    This test is functionally identical to the example in the
    `valid_login.robot`_ file.

`resource.robot`_
    A resource file with reusable keywords and variables.

    The system specific keywords created here form our own
    domain specific language. They utilize keywords provided
    by the imported SeleniumLibrary_.

See `Robot Framework User Guide`_ for more details about the test data syntax.

Generated results
=================

At the end of `running tests`_ report and log in HTML format are Generated.

Running demo
============

Starting demo application
-------------------------

You can start the application either by double clicking
``demoapp/server.py`` file or by executing the command line
shown below::

    python demoapp/server.py

The application is started is available in URL
http://localhost:7272.

Running tests
-------------

The `test cases`_ are located in the ``login_tests`` directory. They can be
executed using the ``robot`` command::

    robot login_tests

Individual test case file can be run by using various command line
options supported by Robot Framework::

    robot login_tests/valid_login.robot
    robot --test InvalidUserName --loglevel DEBUG login_tests

Using different browsers
------------------------

The browser that is used is controlled by ``${BROWSER}`` variable defined in
`resource.robot`_ resource file. Firefox browser is used by default, but that
can be easily overridden from the command line::

    robot --variable BROWSER:Chrome login_tests
    robot --variable BROWSER:IE login_tests

Consult SeleniumLibrary_ documentation about supported browsers.

References
----------

Robot Framework: http://robotframework.org/WebDemo
